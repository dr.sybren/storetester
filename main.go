package main

import (
	"flag"
	"net/http"
	"net/url"
	"time"

	log "github.com/Sirupsen/logrus"
)

const (
	baseURL = "https://store.blender.org/"
	timeout = 5 * time.Second
)

var cliArgs struct {
	verbose bool
	debug   bool
	jsonLog bool
}

func parseCliArgs() {
	flag.BoolVar(&cliArgs.verbose, "verbose", false, "Enable info-level logging")
	flag.BoolVar(&cliArgs.debug, "debug", false, "Enable debug-level logging")
	flag.BoolVar(&cliArgs.jsonLog, "json", false, "Log in JSON format")
	flag.Parse()
}

func configLogging() {
	if cliArgs.jsonLog {
		log.SetFormatter(&log.JSONFormatter{})
	} else {
		log.SetFormatter(&log.TextFormatter{
			FullTimestamp: true,
		})
	}

	// Only log the warning severity or above.
	level := log.WarnLevel
	if cliArgs.debug {
		level = log.DebugLevel
	} else if cliArgs.verbose {
		level = log.InfoLevel
	}
	log.SetLevel(level)
}

func main() {
	parseCliArgs()
	configLogging()
	log.Infof("Store tester for %s", baseURL)

	defer func() {
		// If there was a panic, make sure we log it before quitting.
		if r := recover(); r != nil {
			log.Panic(r)
		}
	}()

	performHTTPRequest("/", 200)
	performHTTPRequest("/does-not-exist-just-testing-here", 404)
}

func performHTTPRequest(testURL string, expectedStatus int) {
	storeURL, err := url.Parse(baseURL)
	if err != nil {
		log.Fatalf("Error parsing '%s' as URL.", baseURL)
		return
	}
	relURL, err := url.Parse(testURL)
	if err != nil {
		log.Fatalf("Error parsing '%s' as URL.", testURL)
		return
	}
	getURL := storeURL.ResolveReference(relURL)

	req, err := http.NewRequest("GET", getURL.String(), nil)
	if err != nil {
		log.Warningf("Unable to create GET request: %s", err)
		return
	}

	log.Infof("Performing GET on %s", getURL)
	client := &http.Client{
		Timeout: timeout,
	}
	resp, err := client.Do(req)
	if err != nil {
		log.Warningf("Unable to GET %s: %s", getURL, err)
		return
	}

	if resp.StatusCode != expectedStatus {
		log.Warningf("Expected status %d but received status %d on URL %v",
			expectedStatus, resp.StatusCode, getURL)
		return
	}

	log.Infof("Received expected status %d", resp.StatusCode)
}
